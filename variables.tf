variable "arn_target" {}
variable "branch_name" {}
variable "enabled" {
  type    = string
  default = "true"
}
variable "postfix" {}
variable "repository_name" {}
variable "role_arn" {}
variable "tags" {}
