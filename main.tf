resource "aws_cloudwatch_event_rule" "console" {
  count         = var.enabled == "true" ? 1 : 0
  description   = "Launch a build on a commit"
  event_pattern = <<PATTERN
  {
  "source": [
    "aws.codecommit"
  ],
  "detail-type": [
    "CodeCommit Repository State Change"
  ],
  "detail": {
    "event": [
      "referenceCreated",
      "referenceUpdated"
    ],
    "repositoryName": [
      "${var.repository_name}"
    ],
    "referenceType": [
      "branch"
    ],
    "referenceName": [
      "${var.branch_name}"
    ]
  }
}
PATTERN
  name          = "${var.repository_name}-${var.postfix}"
  tags          = var.tags
}
resource "aws_cloudwatch_event_target" "deploy" {
  arn      = var.arn_target
  count    = var.enabled == "true" ? 1 : 0
  role_arn = var.role_arn
  rule     = aws_cloudwatch_event_rule.console[count.index].name
}
