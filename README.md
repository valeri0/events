## Requirements

| Name | Version |
|------|---------|
| aws | ~> 3.9.0 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 3.9.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| arn\_target | n/a | `any` | n/a | yes |
| branch\_name | n/a | `any` | n/a | yes |
| enabled | n/a | `string` | `"true"` | no |
| postfix | n/a | `any` | n/a | yes |
| repository\_name | n/a | `any` | n/a | yes |
| role\_arn | n/a | `any` | n/a | yes |
| tags | n/a | `any` | n/a | yes |

## Outputs

No output.
